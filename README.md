# dbox2cru
Python utility to download directory's from dropbox.  What makes this tool a bit different is that you can configure the writer (in the defs.ini file) to write to multiple locations at download time.  The original intent was to allow downloading to a local HDD (ie NAS/raid/fiber or whatever) while also making a copy to an externally attached media (ie a DCI CRU).  In short, we've pipelined the process of making sure we have the clients data on our mass storage device, while also being able to have the CRU immediately ready for delivery to a theater.

The downloader is currently somewhat dumb   Here's a few notes about it:
* It's smart enough to know if a download was not completed last time, ie will pick up where it left off (or trys to figure out how to)
* It won't notice if you've got stuff in the destinatino that does not belong (ie files which no longer exist on the source, ie not a true mirror process, we simply add more to what is there)
* It will create the necessary directory structures (like mkdir -p does) 
* It does not do checksums (yet, ie ETAG variant as used by dropbox)

Pre-req
 * pip install dropbox
 * pycurl 
 
 Copy defs.ini.example to defs.ini

 Edit defs.ini replacing SETTODROPBOXTOKEN with your real dropbox box token

 (You can retrieve your token from your autodcp edit/settings page)