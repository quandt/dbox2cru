#!/usr/bin/env /usr/bin/python

import os
import json 
import boto3
import sys

from clairmeta import DCP

tstdir = "/mnt/festivalheaven/Drop This/DropBox/Apps/AutoDCP/deliverables/peff2019/cru0/mnarimanov@mail.ru/"
#tstdir = "/mnt/festivalheaven/Drop This/DropBox/Apps/AutoDCP/deliverables/peff2019/cru0/"

session = boto3.Session(
        #aws_access_key_id = config[defsection]['awsid'],
        #aws_secret_access_key = config[defsection]['awssecret'],
        region_name='us-west-2',
    )
dynamodb = session.resource('dynamodb')
table = dynamodb.Table('xxx')

dcpfolders = []
for root,dirs,files in os.walk(tstdir):
    if not dirs:
        dcpfolders.append(root)

dcps = []
for dcpdir in dcpfolders:
    try:
      dcp = DCP(dcpdir) 
      dcp.parse()
      status,report = dcp.check()
      report['dir'] = dcpdir
      report['Id'] = dcp.list_cpl[0]['Info']['CompositionPlaylist']['Id']
      #report['#key1'] = dcp.list_cpl[0]['Info']['CompositionPlaylist']['Id']
      report['ContentTitleText'] = dcp.list_cpl[0]['Info']['CompositionPlaylist']['ContentTitleText']
      dcps.append(report)
      print json.dumps(report)
      print(os.stat(report['dir']).st_ino, report['dir'])
      table.put_item(
          Item = {
       	      'Id' : report['Id'],
       	      'INFO' : json.dumps(report['INFO']),
       	      'ERROR' : json.dumps(report['ERROR']),
       	      'WARNING' : json.dumps(report['WARNING']),
       	      'dir' : report['dir'],
       	      'inode' : os.stat(dcpdir).st_ino,
       	      'ContentTitleText' : report['ContentTitleText'],
              }
          )
    except:
      print "%s skipping " % (dcpdir)
    sys.exit()

print json.dumps(dcps)


