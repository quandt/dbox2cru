#!/usr/bin/env python
import argparse
import pycurl,json
import sys
import signal
import configparser
import dropbox
import os
import errno
import json

class mywrite():
    def __init__(self,outputs=[]):
        #fh = sys.stdout
        self.fptrs = []

        #make my dirs if they don't already exist
        for filename in outputs:
            xx = os.path.abspath(filename) #deal with common case of handling local paths
            if not os.path.exists(os.path.dirname(xx)):
                try:
                    os.makedirs(os.path.dirname(xx))
                except OSError as exc: 
                    if exc.errno != errno.EEXIST:
                        raise

        #And now the files themselves
        for filename in outputs:
            try:
                self.fptrs.append(open(filename,'rb+'))
            except IOError as exc: 
                if exc.errno == errno.EEXIST:
                    raise
                self.fptrs.append(open(filename,'wb+'))

    def writ(self,buf):
        #sys.stdout.write(buf)
        for x in self.fptrs:
            x.write(buf)

    def clos(self,interactive=False):
        if (interactive):
            sys.stderr.write("\nFlushing cache, please wait...")
        for x in self.fptrs:
            x.truncate()
            x.flush()
            x.close()
        if (interactive):
            sys.stderr.write("\ncache cleared.\n")

    #
    # Look at all our outputs, and set the eof ptr to shortest one, 
    # ie retrieve data starting there if unfinished from prior run
    #
    def seteof(self):
        eofptr = sys.maxint
        
        #find our least EOF
        for x in self.fptrs:
            x.seek(0,2)
            if x.tell() < eofptr:
                eofptr = x.tell()

        #set all our output files to the least EOF of all the files so we pick up at the same place upon appending
        for x in self.fptrs:
            x.seek(eofptr,0)
        return eofptr 


class sighndlr():
    def __init__(self,writer):
        self.writer = writer

    def signal_handler(self,sig, frame):
        sys.stderr.write("You presssed control-c")
        self.writer.clos(interactive=True)
        sys.stderr.write("\nDownloads may not have completed.\nYou will need to run this utilty again to complete the download\n")
        sys.exit(0)

class dbox:
    def __init__(self,dboxtoken):
        self.dboxtoken = dboxtoken
        x=1

    def download(self,filepath,writer,start):
        url="https://content.dropboxapi.com/2/files/download"
        curl = pycurl.Curl()
        curl.setopt(curl.URL, url)
        curl.setopt(pycurl.HTTPHEADER,['Content-Type: application/json'])
        curl.setopt(curl.HTTPHEADER, ["Authorization: Bearer %s" % (self.dboxtoken) ,
                                "Dropbox-API-Arg: {\"path\": \"%s\"}" % (filepath) ])
        curl.setopt(curl.RESUME_FROM, start)
        curl.setopt(curl.WRITEFUNCTION, writer)
        curl.setopt(curl.BUFFERSIZE, 524288)
        curl.perform()
        curl.close()


parser = argparse.ArgumentParser(description="Utilty to download a festival directory from your dropbox account, and simultaneously  mirror to an attached CRU.")
parser.add_argument("--festival", help="If specified, instead of processing what is listed in the default section of defs.ini, process this instead")
args = parser.parse_args()

defsection = "default"
if args.festival:
    defsection = args.festival

config = configparser.ConfigParser()
config.read('defs.ini')

localdest = json.loads(config[defsection]['localdest'])

interactive = True
if (interactive):
    print "Processing defs.ini section %s" % (defsection)

dbx = dropbox.Dropbox(config[defsection]['dboxtoken'])
response = dbx.files_list_folder(path=config[defsection]['dboxsrcpath'],recursive=True)
for entry in response.entries:
    #Only handle the files, dirs are auto created by our writer just like mkdir -p does
    if (isinstance(entry, dropbox.files.FileMetadata) and (entry.size < 20000000000)):
        if (interactive):
            print "\t%s" %(entry.path_display)
        newlocaldest = []
        for x in localdest:
            newlocaldest.append("%s%s"%(x,entry.path_display))
        mptr = mywrite(newlocaldest)
        starting_point =  mptr.seteof()

        hndlr = sighndlr(mptr)
        signal.signal(signal.SIGINT, hndlr.signal_handler)

        mdbx = dbox(config[defsection]['dboxtoken'])
        mdbx.download(entry.path_display,mptr.writ,starting_point)
        mptr.clos()
    else:
        print "skipping\t%s" %(entry.path_display)

while response.has_more:
    response = dbx.files_list_folder_continue(response.cursor)
    for entry in response.entries:
        #Only handle the files, dirs are auto created by our writer just like mkdir -p does
        if (isinstance(entry, dropbox.files.FileMetadata) and (entry.size < 20000000000)):
            if (interactive):
                print "\t%s" %(entry.path_display)
            newlocaldest = []
            for x in localdest:
                newlocaldest.append("%s%s"%(x,entry.path_display))
            mptr = mywrite(newlocaldest)
            starting_point =  mptr.seteof()

            hndlr = sighndlr(mptr)
            signal.signal(signal.SIGINT, hndlr.signal_handler)
        
            mdbx = dbox(config[defsection]['dboxtoken'])
            mdbx.download(entry.path_display,mptr.writ,starting_point)
            mptr.clos()
    else:
            print "skipping\t%s" %(entry.path_display)


if (interactive):
    print "\nFinished"
